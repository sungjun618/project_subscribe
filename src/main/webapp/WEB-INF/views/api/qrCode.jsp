<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<script src="<c:url value='/resources/js/jquery-3.4.1.js'/>"></script>
	<script src="<c:url value='/resources/qr/jquery.qrcode.js'/>"></script>
	<script src="<c:url value='/resources/qr/qrcode.js'/>"></script>
<title>Insert title here</title>
<script>
$(function(){
	$('#qrcodeTable').qrcode({
		render	: "table",
		text	: "<c:url value='/'/>"
	});	
})
</script>
</head>
<body>
	<div id="qrcodeTable"></div>
	
</body>
</html>