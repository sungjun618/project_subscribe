<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<script src="<c:url value='/resources/js/jquery-3.4.1.js'/>"></script>
<title>Insert title here</title>
<script>
// function onopen() {
// // var url =
// // "http://www.ftc.go.kr/bizCommPop.do?wrkr_no="+frm1.wrkr_no.value;
// // var test = window.open(url, "bizCommPop", "width=750, height=700;");

// // setTimeout(function(){
// // console.log(test.document);	
// // },2000);
// // // test.close();
// 	var num22 = $('input[name=wrkr_no]').val();
// 	num22 = CRNum2Num(num22);
// 	console.log(num22);
// 	var flag = ValidCorpNum(num22);
// 	console.log(flag);
// }

// //-----------------------------------------------------------------------
// // 기  능 : 숫자인지 여부 반환
// //-----------------------------------------------------------------------
// function isNumeric(s) {
// 	for (i=0; i<s.length; i++) {
// 		c = s.substr(i, 1);
// 		if (c < "0" || c > "9") return false;
// 	}
// 	return true;
// }
// //-----------------------------------------------------------------------
// // 기  능 : 숫자를 금액 형식으로
// //-----------------------------------------------------------------------
// function Num2Money(number){

// 	number		=	Money2Num(number);

// 	if(isNaN(number))			return number;
// 	if(parseFloat(number)==0)	return 0;
// 	if(!number)					return "";
// 	var strint="";
// 	var strfloat="";
// 	var tmp;
// 	var ans="";
// 	var isfloat=false;
// 	var minus=false;
// 	var result='';

// 	if(number.indexOf("-")>-1){
// 		minus=true;
// 		number=number.replace('-','');
// 	}

// 	if(number.indexOf(".")==-1) strint=number;
// 	else {
// 		isfloat=true;
// 		strint=number.substring(0,number.indexOf("."));
// 		if(parseInt(strint,10)==0)	strint = '0';
// 		strfloat=number.substring(number.indexOf("."),number.length);
// 	}

// 	if(!isNaN(strint) && strint!=""){
// 		strint	=	String(parseInt(strint,10));
// 	}

// 	var num=strint.split("");

// 	tmp=num.reverse();

// 	for(a=0;a<tmp.length;a++) {
// 		if(!(a%3) && a!=0) { ans+=",";  }
// 		ans+=tmp[a];
// 	}

// 	tmp=ans.split("").reverse();
// 	ans="";

// 	for(a=0;a<tmp.length;a++){ ans+=tmp[a];}

// 	for(a=0;a<2;a++)
// 		if(ans.charAt(0)=="0" || ans.charAt(0)==",") ans=ans.substring(1,ans.length);

// 	result=ans+strfloat;

// 	if(isfloat==true){
// 		var lastchar=result.substr(result.length-1,1);
// 		while (lastchar == '0' || lastchar == '.'){
// 			result = result.substr(0,result.length-1);
// 			if (lastchar == '.'){break;}
// 			lastchar=result.substr(result.length-1,1);
// 		}
// 	}

// 	var tmpresult = parseFloat(Money2Num(result));

// 	if(tmpresult>0 && tmpresult<1){
// 		result = '0'+result;
// 	}

// 	return	(minus==true) ? '-'+result : result
// }
// //-----------------------------------------------------------------------
// // 기  능 : 금액을 숫자 형식으로
// //-----------------------------------------------------------------------
// function Money2Num(money) {
// 	if(money == undefined)	return false;
// 	var moneyString		=	new String;
// 	moneyString			=	money.toString();
// 	while (moneyString.indexOf(',') > -1){
// 		moneyString	=	moneyString.replace(',','');
// 	}
// 	if(isNaN(moneyString)){
// 		return false;
// 	}else{
// 		return moneyString;
// 	}
// }
// //-----------------------------------------------------------------------
// // 기  능 : 올바른 사업자번호인지 확인
// //-----------------------------------------------------------------------
function ValidCorpNum(Num){
	Num = $.trim(Num);
	baroVal = 0;
	comStr = "13713713";

	for( i = 0 ; i < 8 ; i++ ) {
		baroVal = baroVal + (parseFloat(Num.substring(i,i+1))*parseFloat(comStr.substring(i,i+1))) % 10;
	}

	tmpComp = parseFloat(Num.substring(8,9)) * 5 + "0";
	chkValue = parseFloat(tmpComp.substring(0,1)) + parseFloat(tmpComp.substring(1,2));
	chkDigit = (10-(baroVal+chkValue) % 10 ) % 10;

	if( Num.substring(9,10) != chkDigit ) {
		if( Num == "1328302034" || Num == "1298301331" || Num == "6198300414" || Num == "2128177509" || Num == "1278301558" ) {
			return true;
		} else {
			return false;
		}
	} else {
		return true;
	}
}
// //-----------------------------------------------------------------------
// // 기  능 : 올바른 주민등록번호인지 확인
// //-----------------------------------------------------------------------
// function ValidResiNum(resiNum){
// 	if(resiNum=='9999999999999')
// 		return true;

// 	resiNum = $.trim(resiNum);

// 	var isForeigner = parseInt(resiNum.substr(6,1),10) >= 5;
// 	if (!isForeigner) {
// 		var lastid, li_mod, li_minus, li_last;
// 		var checkValue = 0;

// 		if (resiNum.length != 13)
// 			return false;

// 		lastid = parseFloat(resiNum.substring(12,13));
// 		checkValue += (parseInt(resiNum.substring(0,1)) * 2) + (parseInt(resiNum.substring(1,2)) * 3)
// 			+ (parseInt(resiNum.substring(2,3)) * 4) + (parseInt(resiNum.substring(3,4)) * 5)
// 			+ (parseInt(resiNum.substring(4,5)) * 6) + (parseInt(resiNum.substring(5,6)) * 7)
// 			+ (parseInt(resiNum.substring(6,7)) * 8) + (parseInt(resiNum.substring(7,8)) * 9)
// 			+ (+parseInt(resiNum.substring(8,9)) * 2)  + (parseInt(resiNum.substring(9,10)) * 3)
// 			+ (parseInt(resiNum.substring(10,11)) * 4) + (parseInt(resiNum.substring(11,12)) * 5);

// 		li_mod = checkValue % 11;
// 		li_minus = 11 - li_mod;
// 		li_last = li_minus % 10;

// 		if (li_last != lastid)
// 			return false;
// 	} else {
// 		var sum = 0;
// 		var odd = 0;

// 		buf = new Array(13);

// 		for(i=0; i<13; i++)
// 			buf[i] = parseInt(resiNum.charAt(i));

// 		odd = buf[7]*10 + buf[8];

// 		if(odd%2 != 0)
// 			return false;

// 		if((buf[11]!=6) && (buf[11]!=7) && (buf[11]!=8) && (buf[11]!=9))
// 			return false;

// 		multipliers = [2,3,4,5,6,7,8,9,2,3,4,5];

// 		for(i=0, sum=0; i<12; i++)
// 			sum += (buf[i] *= multipliers[i]);

// 		sum = 11 - (sum%11);
// 		if(sum >= 10)
// 			sum -= 10;

// 		sum += 2;
// 		if(sum >= 10)
// 			sum -= 10;

// 		if(sum != buf[12])
// 			return false;
// 	}

// 	return true;
// }
// //-----------------------------------------------------------------------
// // 기  능 : 사업자번호에 '-' 추가
// //-----------------------------------------------------------------------
// function Num2CorpNum(num){
// 	if(isNaN(num)){
// 		return num;
// 	}
// 	num		=	String(num);
// 	if(num == ""){
// 		return num;
// 	}else{
// 		if(num.indexOf("-") == -1){
// 			if(String(num).length == 10){
// 				return num.substr(0,3)+"-"+num.substr(3,2)+"-"+num.substr(5,5);
// 			}else{
// 				return num;
// 			}
// 		}else{
// 			return num;
// 		}
// 	}
// }
// //-----------------------------------------------------------------------
// // 기  능 : 주민등록번호에 '-' 추가
// //-----------------------------------------------------------------------
// function Num2ResiNum(num){
// 	if(isNaN(num)){
// 		return num;
// 	}
// 	num		=	String(num);
// 	if(num == ""){
// 		return num;
// 	}else{
// 		if(num.indexOf("-") == -1){
// 			if(String(num).length == 13){
// 				return num.substr(0,6)+"-"+num.substr(6,7);
// 			}else{
// 				return num;
// 			}
// 		}else{
// 			return num;
// 		}
// 	}
// }
// //-----------------------------------------------------------------------
// // 기  능 : 사업자번호, 주민등록번호 판별하여 '-' 추가
// //-----------------------------------------------------------------------
// function Num2CRNum(num){
// 	switch (num.length){
// 	case 10:	return Num2CorpNum(num);	break;
// 	case 13:	return Num2ResiNum(num);	break;
// 	default:	return num;					break;
// 	}
// }
// //-----------------------------------------------------------------------
// // 기  능 : 사업자번호, 주민등록번호의 '-' 제거
// //-----------------------------------------------------------------------
function CRNum2Num(num){
	newnum	=	num.replace(/-/g,'');
	if(isNaN(newnum)){
		return "";
	}else{
		return newnum;
	}
}
// -------------------------------------------------
// -----------------공공정보포탈 api -------------------
// ----★★★★★★★★★★★★★★★★★★★★★★★★★사업자 번호로 정보 뽑기
function onopen() {

// 	----------[입력한 숫자 들고온 후  -제거]
	var num22 = $('input[name=wrkr_no]').val();
	num22 = CRNum2Num(num22);


	var xhr = new XMLHttpRequest();
	var url = 'http://apis.data.go.kr/1130000/MllInfoService/getMllInfo'; /*URL*/
	var queryParams = '?' + encodeURIComponent('serviceKey') + '='+'ou3lqGyduwidbSq6yAaEjfD0bMuvnbGY%2BCqwIAyrnY8ToivtdH%2BphnaWila2X4628d%2FKs%2FZDoURgGkC10P%2BCTw%3D%3D'; /*Service Key*/
// 	queryParams += '&' + encodeURIComponent('sidoNm') + '=' ;
// 	+ encodeURIComponent('경상북도'); /*시도명*/
// 	queryParams += '&' + encodeURIComponent('cggNm') + '=' ;
// 	+ encodeURIComponent('구미'); /*시군구명*/
// 	queryParams += '&' + encodeURIComponent('mngStateCode') + '=' ;
// 	+ encodeURIComponent('01'); /*영업상태코드(01:'통신판매업 신고' 02:'통신판매업 휴업' 03:'통신판매업 폐업' 04:'직권취소' 05:'타시군구이관' 06:'타시군구전입' 07:'직권말소' 08:'영업재개')*/
// 	queryParams += '&' + encodeURIComponent('fromPermYmd') + '=' ;
// 	+ encodeURIComponent('20110525'); /*신고일자(검색시작일)*/
// 	queryParams += '&' + encodeURIComponent('toPermYmd') + '=' ;
// 	+ encodeURIComponent('20150525'); /*신고일자(검색종료일)*/
// 	queryParams += '&' + encodeURIComponent('bupNm') + '=' ;
// 	+ encodeURIComponent('㈜공공데이터'); /*상호*/
	queryParams += '&' + encodeURIComponent('wrkrNo') + '=' + encodeURIComponent(num22); /*사업자등록번호*/
	console.log(num22);
// 	queryParams += '&' + encodeURIComponent('repsntNm') + '=' ;
// 	+ encodeURIComponent('홍길동'); /*대표자*/
	xhr.open('GET', url + queryParams);

// 	xhr.setRequestHeader('Access-Control-Allow-Headers', '*');
// 	xhr.setRequestHeader('Content-type', 'application/ecmascript');
// 	xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
	
	xhr.onreadystatechange = function () {
	    if (this.readyState == 4) {
// 	        console.log('Status: '+this.status+' Headers: '+JSON.stringify(this.getAllResponseHeaders())+' Body: '+this.responseText);
// 	        console.log(this);
	        var text = this.responseXML;
	        var bossName = text.getElementsByTagName('repsntNm')[0];
	        if(bossName == undefined) {
				console.log("안됨");
		    } else {
			    console.log(bossName.childNodes[0].nodeValue);
			}
	    }
	};
	
	xhr.send('');
	
}
</script>
</head>
<body>
<form name="frm1">
<input name="wrkr_no" type="text" value="211-88-68802"/>
<input type="button" value="팝업" onclick="onopen();"/>
</form>
</body>
</html>