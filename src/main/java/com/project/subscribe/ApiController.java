package com.project.subscribe;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ApiController {

	@RequestMapping(value = "api/businessNo", method = RequestMethod.GET)
	public String businessNo() {
		return "api/businessNo";
	}
	
	@RequestMapping(value = "api/canvas", method = RequestMethod.GET)
	public String canvas() {
		return "api/canvas";
	}
	
	@RequestMapping(value = "api/qrCode", method = RequestMethod.GET)
	public String qrCode() {
		return "api/qrCode";
	}
}
