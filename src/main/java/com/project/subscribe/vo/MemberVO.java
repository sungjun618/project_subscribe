package com.project.subscribe.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberVO {

	private String memberAuthKey;
	private String memberPassword;
	private String memberEmail;
	private String userState;
}
